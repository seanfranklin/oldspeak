/**
 * Content script for Oldspeak Chrome extension.
 */

/* globals osDictionary */
'use strict';

const osContent = {

    originalBody:  document.body.innerHTML,
    oldspeakSpans: document.getElementsByClassName('oldspeak'),
    newspeakSpans: document.getElementsByClassName('newspeak'),


    power(state) {
        console.log(`content.power(${state})`);
        if (state) {
            let workingBody = osContent.originalBody;
            for (let i = 0; i !== osDictionary.length; ++i) {
                const newspeak = osDictionary[i][0],
                      oldspeak = osDictionary[i][1],
                        regexp = new RegExp(newspeak, 'gi');
                workingBody = workingBody.replace(regexp,
                   `<span class="newspeak">$&</span><span class="newspeak newspeak-space"> </span><span class="oldspeak">${oldspeak}</span>`);
            }
            document.body.innerHTML = workingBody;
        } else if (document.body.innerHTML !== osContent.originalBody) {
            document.body.innerHTML = osContent.originalBody;
        }
        chrome.runtime.sendMessage({ option: 'highlight' }, response => {});
        chrome.runtime.sendMessage({ option: 'original' }, response => {});
    },


    highlight(state) {
        console.log(`content.highlight(${state})`);
        for (let span of osContent.oldspeakSpans) {
            span.classList[state ? 'add' : 'remove']('lightspeak');
        }
    },


    original(state) {
        console.log(`content.original(${state})`);
        for (let span of osContent.newspeakSpans) {
            span.classList[state ? 'remove' : 'add']('hidespeak');
        }
    }

};


// Listen for and respond to option changes
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    console.info('Event: message from background script');
    console.log(`request.option: ${request.option}`);
    console.log(`request.state: ${request.state}`);
    osContent[request.option](request.state);
});

// Apply selected extension options to page
chrome.runtime.sendMessage({ option: 'power' }, response => {});
