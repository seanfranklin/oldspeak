/**
 * Background script for Oldspeak Chrome extension.
 */

'use strict';

var osBackground = {

    storage:   chrome.storage.local,
    power:     false,
    highlight: false,
    original:  false,


    // Sends request to content script to apply specified option
    applyOption(option, state) {
        console.log(`osBackground.applyOption(${option}, ${state})`);
        chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
            chrome.tabs.sendMessage(tabs[0].id, { option, state }, response => {});
        });
    },


    // Toggles state of specified option and save to storage
    toggleOption(option) {
        console.log(`osBackground.toggleOption(${option})`);
        osBackground[option] = !osBackground[option];        
        osBackground.storage.set({ [option]: osBackground[option] }, () => {});
        osBackground.applyOption(option, osBackground[option]);
    }

};


// Get option states from storage
osBackground.storage.get(null, storedState => {
    if (storedState.power) { osBackground.power = storedState.power; }
    if (storedState.highlight) { osBackground.highlight = storedState.highlight; }
    if (storedState.original) { osBackground.original = storedState.original; }
});


chrome.extension.onMessage.addListener((request, sender, sendResponse) => {
    console.info('Event: message from content script');
    console.log(`request.option: ${request.option}`);
    osBackground.applyOption(request.option, osBackground[request.option]);
});
