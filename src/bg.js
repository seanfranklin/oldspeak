/**
 * Background script for Oldspeak Chrome extension.
 */

'use strict';

const bg = {

    storage:   chrome.storage.local,
    power:     false,
    highlight: false,
    original:  false,


    // Sends request to content script to apply specified option
    applyOption(option, state) {
        console.log(`bg.applyOption(${option}, ${state})`);
        chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
            chrome.tabs.sendMessage(tabs[0].id, { option, state }, response => {});
        });
    },


    // Toggles state of specified option and save to storage
    toggleOption(option) {
        console.log(`bg.toggleOption(${option})`);
        bg[option] = !bg[option];        
        bg.storage.set({ [option]: bg[option] }, () => {});
        bg.applyOption(option, bg[option]);
    }

};


// Get option states from storage
bg.storage.get(null, storedState => {
    if (storedState.power) bg.power = storedState.power;
    if (storedState.highlight) bg.highlight = storedState.highlight;
    if (storedState.original) bg.original = storedState.original;
});


chrome.extension.onMessage.addListener((request, sender, sendResponse) => {
    console.info('Event: message from content script');
    console.log(`request.option: ${request.option}`);
    bg.applyOption(request.option, bg[request.option]);
});
