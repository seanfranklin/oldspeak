/**
 * Popup script for Oldspeak Chrome extension.
 */

'use strict';

const osPopup = {
    bgPage:    chrome.extension.getBackgroundPage(),
    power:     document.getElementById('power'),
    highlight: document.getElementById('highlight'),
    original:  document.getElementById('original')
};

// Display correct option states
osPopup.power.src = osPopup.bgPage.osBackground.power ? 'on.png' : 'off.png';
osPopup.highlight.checked = osPopup.bgPage.osBackground.highlight;
osPopup.original.checked = osPopup.bgPage.osBackground.original;

document.addEventListener('DOMContentLoaded', () => {
    osPopup.power.addEventListener('click', () => {
        osPopup.bgPage.console.info('Event: click on power');
        osPopup.bgPage.osBackground.toggleOption('power');
        osPopup.power.src = osPopup.bgPage.osBackground.power ? 'on.png' : 'off.png';
    });
    osPopup.highlight.addEventListener('change', () => {
        osPopup.bgPage.console.info('Event: click on highlight');
        osPopup.bgPage.osBackground.toggleOption('highlight');
    });
    osPopup.original.addEventListener('change', () => {
        osPopup.bgPage.console.info('Event: click on original');
        osPopup.bgPage.osBackground.toggleOption('original');
    });
});
