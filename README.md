Chrome extension that replaces euphemistic words and phrases with clear language.
Note: Allow access to file URLs if you want the extension to work on local files (such as the test page).
